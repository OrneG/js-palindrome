function isPalindrome(str) {
    //Creo la palabra sin espacios ni mayúsculas
    let palindrome = str.toLowerCase().split(' ').join('');
    //Verifico que el parámetro sea un string
    if (typeof str != 'string') {
        throw console.error('Not a palindrome');
    }
    //Comparo con la palabra al revéz
    return palindrome.split('').reverse().join('') === palindrome;
}

module.exports = isPalindrome